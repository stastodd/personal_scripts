import base64
import sys
from binascii import unhexlify
# from pdb import set_trace
# from pprint import pprint

try:
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
except ModuleNotFoundError as e:
    print(e)
    print("You have to use: 'pip install <packetname>'")
    sys.exit()


"""
Name: lora_decode.py
Version: 0.1
Use some block of code: https://github.com/jieter/python-lora
Alternative result check: https://lorawan-packet-decoder-0ta6puiniaut.runkit.sh/
Author: Stas Goncharenko

Script for decoding messages from IoT devices.

SCRIPT ARGUMENTS:
lora_decode.py has arguments: 'appskey | dev_addr | sequence_counter | pkt_direction | packet_data'
 appskey - Application session key (AppSKey) [128 bit] used to encrypt data at the application level (between the 
terminal and the application server).
 dev_addr - Local address of the device in this network [32 bits].
 sequence_counter - FCntUp.
 pkt_direction - 0 for uplink packets, 1 for downlink packets.
 packet_data - payload in Base64 format.

HOW WORK WITH IT?
0) Prepare and activate python3.6+ environment. Install 'cryptography' lib. 
1) Setup and activate GW with SmartRoomSensor (maybe can work with other IoT, not tested) manually at NetworkServer.
2) Run script using line: 
python lora_decode.py 67efd581c0e3c5c8677e5beba8164b5f 074ca65c 277 0 QFymTAeCFQEDBwqs3DreKgHN1Vxkt3ef6fI=
3) Work with data format: [[3, 103, 0, 243, 4, 104, 76, 0, 255, 1, 46], '036700F304684C00FF012E']
"""


def to_bytes(s):
    """
    PY2/PY3 compatible way to convert to something cryptography understands
    """
    if sys.version_info < (3,):
        return "".join(map(chr, s))
    else:
        return bytes(s)


def loramac_decrypt(payload_hex, sequence_counter, key, dev_addr, direction=0):
    """
    LoraMac decrypt

    Which is actually encrypting a predefined 16-byte block (ref LoraWAN
    specification 4.3.3.1) and XORing that with each block of data.

    payload_hex: hex-encoded payload (FRMPayload)
    sequence_counter: integer, sequence counter (FCntUp)
    key: 16-byte hex-encoded AES key. (i.e. AABBCCDDEEFFAABBCCDDEEFFAABBCCDD)
    dev_addr: 4-byte hex-encoded DevAddr (i.e. AABBCCDD)
    direction: 0 for uplink packets, 1 for downlink packets

    returns structure: [[3, 103, 0, 243, 4, 104, 76, 0, 255, 1, 46], '036700F304684C00FF012E'].

    This method is based on `void LoRaMacPayloadEncrypt()` in
    https://github.com/Lora-net/LoRaMac-node/blob/master/src/mac/LoRaMacCrypto.c#L108
    """
    sequence_counter = int(sequence_counter)
    direction = int(direction)
    key = unhexlify(key)
    dev_addr = unhexlify(dev_addr)
    buffer = bytearray(unhexlify(payload_hex))
    size = len(buffer)

    bufferIndex = 0
    # block counter
    ctr = 1

    # output buffer, initialize to input buffer size.
    encBuffer = [0x00] * size

    cipher = Cipher(algorithms.AES(key), modes.ECB(), backend=default_backend())

    def aes_encrypt_block(aBlock):
        """
        AES encrypt a block.
        aes.encrypt expects a string, so we convert the input to string and
        the return value to bytes again.
        """
        encryptor = cipher.encryptor()

        return bytearray(encryptor.update(to_bytes(aBlock)) + encryptor.finalize())

    # For the exact definition of this block refer to
    # 'chapter 4.3.3.1 Encryption in LoRaWAN' in the LoRaWAN specification
    aBlock = bytearray(
        [
            0x01,  # 0 always 0x01
            0x00,  # 1 always 0x00
            0x00,  # 2 always 0x00
            0x00,  # 3 always 0x00
            0x00,  # 4 always 0x00
            direction,  # 5 dir, 0 for uplink, 1 for downlink
            dev_addr[3],  # 6 devaddr, lsb
            dev_addr[2],  # 7 devaddr
            dev_addr[1],  # 8 devaddr
            dev_addr[0],  # 9 devaddr, msb
            sequence_counter & 0xFF,  # 10 sequence counter (FCntUp) lsb
            (sequence_counter >> 8) & 0xFF,  # 11 sequence counter
            (sequence_counter >> 16) & 0xFF,  # 12 sequence counter
            (sequence_counter >> 24) & 0xFF,  # 13 sequence counter (FCntUp) msb
            0x00,  # 14 always 0x01
            0x00,  # 15 block counter
        ]
    )

    # complete blocks
    while size >= 16:
        aBlock[15] = ctr & 0xFF
        ctr += 1
        sBlock = aes_encrypt_block(aBlock)
        for i in range(16):
            encBuffer[bufferIndex + i] = buffer[bufferIndex + i] ^ sBlock[i]

        size -= 16
        bufferIndex += 16

    # partial blocks
    if size > 0:
        aBlock[15] = ctr & 0xFF
        sBlock = aes_encrypt_block(aBlock)
        for i in range(size):
            encBuffer[bufferIndex + i] = buffer[bufferIndex + i] ^ sBlock[i]

    encBuffer_hex = "".join([hex(x)[2:].zfill(2) for x in encBuffer]).upper()
    return [encBuffer, encBuffer_hex]


def phypayload_splitting(phypayload):
    """
    :param phypayload: "405ca64c0782150103070aacdc3ade2a01cdd55c64b7779fe9f2"
    :return dict
    """
    preambula = phypayload[:2]
    macpayload = phypayload[2:44]
    fhdr = macpayload[:18]
    fport = macpayload[18:20]
    frmpayload = macpayload[20:]

    return {
        "preambula": preambula,
        "macpayload": macpayload,
        "fhdr": fhdr,
        "fport": fport,
        "frmpayload": frmpayload
    }


def main(phypayload_data, sequence_counter, key, dev_addr, pkt_direction):
    payload = phypayload_data.get("frmpayload")
    data = loramac_decrypt(payload, sequence_counter, key, dev_addr, direction=pkt_direction)
    print(f"data: {str(data)}")


if __name__ == '__main__':
    all_args = sys.argv[1:]
    if len(all_args) != 5:
        print(f"Script stopped. Need 5 arguments:\n'appskey | dev_addr | sequence_counter | pkt_direction | packet_data'")
        sys.exit()

    appskey = all_args[0]
    dev_addr = all_args[1]
    sequence_counter = all_args[2]
    pkt_direction = all_args[3]
    packet_data = all_args[4]

    phypayload = base64.b64decode(packet_data).hex()
    phypayload_data = phypayload_splitting(phypayload)
    main(phypayload_data, sequence_counter, appskey, dev_addr, pkt_direction)
